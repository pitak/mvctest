﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TLEFreelance.Models
{
    public class CustomerInputModels
    {
        [Required]
        public string CustomerCode { get; set; }
        [Required]
        public string FistName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public int IDCrad { get; set; }
        [Required]
        public string Tel { get; set; }
        [Required]
        public int IsActive { get; set; }
    }
}