﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TLEFreelance.Models
{
    public class chawsuanObj
    {
        public class clsRetCustomerCode
        {
            public bool oper { get; set; }
            public string retData { get; set; }
            public string retMsg { get; set; }
        }

        public class clsRetProductCode
        {
            public bool oper { get; set; }
            public string retData { get; set; }
            public string retMsg { get; set; }
        }

        public class clsRetOrderCode
        {
            public bool oper { get; set; }
            public string retData { get; set; }
            public string retMsg { get; set; }
        }

        public static clsRetCustomerCode retCustomerCode()
        {
            bool retOper = false;
            string retData = null;
            string retMsg = null;
            try
            {
                DateTime dt = DateTime.Now;
                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.mas_customers select c);
                int CusCount = getData.Count() + 1;
                var newCusCount = CusCount.ToString().PadLeft(4, '0');
                var newMonth = dt.Month.ToString().PadLeft(2, '0');
                retData = "C" + dt.Year.ToString() + newMonth + newCusCount;
                dbConn.Connection.Close();
                retOper = true;
            }
            catch(Exception exp)
            {
                retMsg = exp.Message;
            }
            return new clsRetCustomerCode { oper = retOper, retData = retData, retMsg = retMsg };
        }

        public static clsRetProductCode retProductCode()
        {
            bool retOper = false;
            string retData = null;
            string retMsg = null;
            try
            {
                DateTime dt = DateTime.Now;
                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.mas_products select c);
                int CusCount = getData.Count() + 1;
                var newCusCount = CusCount.ToString().PadLeft(4, '0');
                var newMonth = dt.Month.ToString().PadLeft(2, '0');
                retData = "P" + dt.Year.ToString() + newMonth + newCusCount;
                dbConn.Connection.Close();
                retOper = true;
            }
            catch (Exception exp)
            {
                retMsg = exp.Message;
            }
            return new clsRetProductCode { oper = retOper, retData = retData, retMsg = retMsg };
        }

        public static clsRetOrderCode retOrderCode()
        {
            bool retOper = false;
            string retData = null;
            string retMsg = null;
            try
            {
                DateTime dt = DateTime.Now;
                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.ops_orders select c);
                int oCount = getData.Count() + 1;
                var newOCount = oCount.ToString().PadLeft(4, '0');
                var newMonth = dt.Month.ToString().PadLeft(2, '0');
                retData = "O-" + dt.Year.ToString() + newMonth + newOCount;
                dbConn.Connection.Close();
                retOper = true;
            }
            catch (Exception exp)
            {
                retMsg = exp.Message;
            }
            return new clsRetOrderCode { oper = retOper, retData = retData, retMsg = retMsg };
        }
    }
}