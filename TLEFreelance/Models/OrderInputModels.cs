﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TLEFreelance.Models
{
    public class OrderInputModels
    {
        [Required]
        public string OrderCode { get; set; }
        [Required]
        public string CustomerCode { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public int IsActive { get; set; }
    }
}