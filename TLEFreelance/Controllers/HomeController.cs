﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TLEFreelance.Models;

namespace TLEFreelance.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int page = 1, string SrcType = "Name", string SrcData = null)
        {
            ViewBag.Message = "ข้อมูลการสั่งซือสินค้า";
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            int pageSize = 10;
            int totalPage = 0;
            int totalRecord = 0;
            List<dbDAC.vwOps_Order> allData = new List<dbDAC.vwOps_Order>();
            var getData = (from t in dbConn.vwOps_Orders select t);
            if (!string.IsNullOrEmpty(SrcType) & !string.IsNullOrEmpty(SrcData))
            {
                switch (SrcType)
                {
                    case "Name":
                        getData = getData.Where(d => d.OrderCode.Trim().Contains(SrcData));
                        break;
                }
            }
            totalRecord = getData.Count();
            totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);
            allData = getData.OrderByDescending(a => a.OrderCode).Skip(((page - 1) * pageSize)).Take(pageSize).ToList();
            dbConn.Connection.Close();
            ViewBag.TotalRows = totalRecord;
            ViewBag.PageSize = pageSize;
            return View(allData);
        }

        protected void loadCustomerSlc()
        {
            ViewBag.CustomerSlc = new SelectList(retCustomerItems(), "Value", "Text", null);
           // ViewBag.CustomerSlc ="555";
        }
        protected IEnumerable<SelectListItem> retCustomerItems()
        {
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            List<SelectListItem> Items = new List<SelectListItem>();
            var getData = (from a in dbConn.mas_customers where a.IsActive.Equals(1) select a);
            //Items.Add(new SelectListItem { Text = "NA", Value = "NA" });
            if (getData.Count() > 0)
            {
                foreach (var data in getData)
                {
                    string CusName = "[ " + data.CustomerCode + " ]" + data.FistName + "  " + data.LastName;
                    Items.Add(new SelectListItem { Text = CusName, Value = data.CustomerCode });
                }
            }
            dbConn.Connection.Close();
            return (Items);
        }
        protected void loadAllProduct()
        {
            List<dbDAC.mas_product> prod = new List<dbDAC.mas_product>();
            try
            {
                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from p in dbConn.mas_products where p.IsActive.Equals(1) select p);
                if (getData.Count() > 0)
                {
                    prod = getData.ToList();
                }
                dbConn.Connection.Close();
            }
            catch
            {

            }
            ViewBag.AllProduct = prod;
        }
        protected void loadProductDetail(string orderCode)
        {
            List<dbDAC.ops_orderdetail> prod = new List<dbDAC.ops_orderdetail>();
            try
            {
                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from p in dbConn.ops_orderdetails where p.OrderCode.Equals(orderCode) select p);
                if (getData.Count() > 0)
                {
                    prod = getData.ToList();
                }
                dbConn.Connection.Close();
            }
            catch
            {

            }
            ViewBag.AllProductDetail = prod;
        }

        public ActionResult GetOrderFormNew()
        {
            OrderInputModels model = new OrderInputModels();
            model.OrderCode = "NA";
            model.CustomerCode = "NA";
            model.OrderDate = DateTime.Now;
            model.IsActive = 1;
            this.loadCustomerSlc();
            this.loadAllProduct();
            return PartialView("_LayoutOrderFormNew", model);
        }
        [HttpPost]
        public ActionResult SaveOrderData(OrderInputModels model, FormCollection form)
        {
            //insert database
            bool cbxIsActiveBool = false;
            string retJson = null;
            try
            {
                string CustomerCode = model.CustomerCode;
                DateTime OrderDate = model.OrderDate;
                string cbxIsActive = form["cbxIsActive"];
                var productSlc = form["prodSlc"];
                
                //bool cbxIsActiveBool = Convert.ToBoolean(Request.Form["cbxIsActive"]);
                int IsActive = (cbxIsActive == "false") ? 0 : 1;

                List<dbDAC.ops_orderdetail> tmpOrderDetail = new List<dbDAC.ops_orderdetail>();
                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                dbDAC.ops_order t = new dbDAC.ops_order();
                var operOrderCode = chawsuanObj.retOrderCode();
                if (operOrderCode.oper)
                {
                    string OrderCode = operOrderCode.retData;
                    t.OrderCode = OrderCode;
                    t.CustomerCode = CustomerCode;
                    t.OrderDate = DateTime.Now;
                    t.IsActive = IsActive;
                    dbConn.ops_orders.InsertOnSubmit(t);
                    dbConn.SubmitChanges();
                    
                    string orderCode = t.OrderCode;
                    var productSlcArr = productSlc.Split(',');
                    if (productSlcArr.Count() > 0)
                    {
                        foreach(var spData in productSlcArr)
                        {
                            dbDAC.ops_orderdetail sp = new dbDAC.ops_orderdetail();
                            var productSlcAmount = form["prodAmount_"+ spData+""];
                            sp.OrderCode = OrderCode;
                            sp.ProductCode = spData;
                            sp.QTY = (!string.IsNullOrEmpty(productSlcAmount)) ? Convert.ToInt32(productSlcAmount) : 0;
                            tmpOrderDetail.Add(sp);
                            //retJson += productSlcAmount + ",";
                        }
                        if (tmpOrderDetail.Count() > 0)
                        {
                            dbConn.ops_orderdetails.InsertAllOnSubmit(tmpOrderDetail);
                            dbConn.SubmitChanges();
                        }
                    }
                    dbConn.Connection.Close();
                    return Json(new { success = true, retData = retJson, msg = "Record Inserted!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = operOrderCode.retMsg }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetOrderFormEdit(string orderID)
        {
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            OrderInputModels model = new OrderInputModels();
            model.OrderCode = null;
            model.CustomerCode = null;
            model.OrderDate = DateTime.Now;
            model.IsActive = 1;
            var getData = (from c in dbConn.ops_orders where c.OrderCode.Equals(orderID) select c);
            if (getData.Count() > 0)
            {
                var getDataUse = getData.FirstOrDefault();
                model.OrderCode = getDataUse.OrderCode;
                model.CustomerCode = getDataUse.CustomerCode;
                model.OrderDate = getDataUse.OrderDate.Value;
                model.IsActive = getDataUse.IsActive.Value;
                this.loadProductDetail(getDataUse.OrderCode);
            }
            dbConn.Connection.Close();
            this.loadCustomerSlc();
            this.loadAllProduct();
            return PartialView("_LayoutOrderFormEdit", model);
        }
        [HttpPost]
        public ActionResult UpdateOrderData(OrderInputModels model, FormCollection form)
        {
            //insert database
            bool cbxIsActiveBool = false;
            try
            {
                string OrderCode = model.OrderCode;
                string CustomerCode = model.CustomerCode;
                DateTime OrderDate = model.OrderDate;
                string cbxIsActive = form["cbxIsActive"];
                var productSlc = form["prodSlc"];
                //bool cbxIsActiveBool = Convert.ToBoolean(Request.Form["cbxIsActive"]);
                int IsActive = (cbxIsActive == "false") ? 0 : 1;

                List<dbDAC.ops_orderdetail> tmpOrderDetail = new List<dbDAC.ops_orderdetail>();
                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.ops_orders where c.OrderCode.Equals(OrderCode) select c);
                if (getData.Count() > 0)
                {
                    var getDataUse = getData.FirstOrDefault();
                    getDataUse.OrderCode = OrderCode;
                    getDataUse.CustomerCode = CustomerCode;
                    //getDataUse.OrderDate = OrderDate;
                    getDataUse.IsActive = IsActive;
                    dbConn.SubmitChanges();

                    var productSlcArr = productSlc.Split(',');
                    if (productSlcArr.Count() > 0)
                    {
                        foreach (var spData in productSlcArr)
                        {
                            dbDAC.ops_orderdetail sp = new dbDAC.ops_orderdetail();
                            var productSlcAmount = form["prodAmount_" + spData + ""];
                            sp.OrderCode = OrderCode;
                            sp.ProductCode = spData;
                            sp.QTY = (!string.IsNullOrEmpty(productSlcAmount)) ? Convert.ToInt32(productSlcAmount) : 0;
                            tmpOrderDetail.Add(sp);
                            //retJson += productSlcAmount + ",";
                        }
                        if (tmpOrderDetail.Count() > 0)
                        {
                            var getDeleteData = (from s in dbConn.ops_orderdetails where s.OrderCode.Equals(OrderCode) select s);
                            dbConn.ops_orderdetails.DeleteAllOnSubmit(getDeleteData);

                            dbConn.ops_orderdetails.InsertAllOnSubmit(tmpOrderDetail);
                            dbConn.SubmitChanges();
                        }
                    }
                    dbConn.Connection.Close();
                    return Json(new { success = true, retData = cbxIsActive.ToString(), msg = "Record Update!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = "Data not found!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetOrderFormView(string orderID)
        {
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            OrderInputModels model = new OrderInputModels();
            model.OrderCode = null;
            model.CustomerCode = null;
            model.OrderDate = DateTime.Now;
            model.IsActive = 1;
            var getData = (from c in dbConn.ops_orders where c.OrderCode.Equals(orderID) select c);
            if (getData.Count() > 0)
            {
                var getDataUse = getData.FirstOrDefault();
                model.OrderCode = getDataUse.OrderCode;
                model.CustomerCode = getDataUse.CustomerCode;
                model.OrderDate = getDataUse.OrderDate.Value;
                model.IsActive = getDataUse.IsActive.Value;
                this.loadProductDetail(getDataUse.OrderCode);
            }
            dbConn.Connection.Close();
            this.loadCustomerSlc();
            this.loadAllProduct();
            return PartialView("_LayoutOrderFormView", model);
        }

        public ActionResult GetOrderFormDelete(string orderID)
        {
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            OrderInputModels model = new OrderInputModels();
            model.OrderCode = null;
            model.CustomerCode = null;
            model.OrderDate = DateTime.Now;
            model.IsActive = 1;
            var getData = (from c in dbConn.ops_orders where c.OrderCode.Equals(orderID) select c);
            if (getData.Count() > 0)
            {
                var getDataUse = getData.FirstOrDefault();
                model.OrderCode = getDataUse.OrderCode;
                model.CustomerCode = getDataUse.CustomerCode;
                model.OrderDate = getDataUse.OrderDate.Value;
                model.IsActive = getDataUse.IsActive.Value;
            }
            dbConn.Connection.Close();
            return PartialView("_LayoutOrderFormDelete", model);
        }
        [HttpPost]
        public ActionResult DeleteOrderData(OrderInputModels model, FormCollection form)
        {
            try
            {
                string OrderCode = model.OrderCode;

                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.ops_orders where c.OrderCode.Equals(OrderCode) select c);
                var getSubData = (from c in dbConn.ops_orderdetails where c.OrderCode.Equals(OrderCode) select c);
                if (getData.Count() > 0)
                {
                    var getDataUse = getData.FirstOrDefault();
                    dbConn.ops_orders.DeleteOnSubmit(getDataUse);
                    dbConn.ops_orderdetails.DeleteAllOnSubmit(getSubData);
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();
                    string insID = getDataUse.OrderCode;
                    return Json(new { success = true, retData = insID.ToString(), msg = "Record Deleted!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = "Data not found!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CustomerManage(int page = 1, string SrcType = "Name", string SrcData = null)
        {
            ViewBag.Message = "Customer Management.";
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            int pageSize = 10;
            int totalPage = 0;
            int totalRecord = 0;
            List<dbDAC.mas_customer> allData = new List<dbDAC.mas_customer>();
            var getData = (from t in dbConn.mas_customers select t);
            if (!string.IsNullOrEmpty(SrcType) & !string.IsNullOrEmpty(SrcData))
            {
                switch (SrcType)
                {
                    case "Name":
                        getData = getData.Where(d => d.FistName.Trim().Contains(SrcData));
                        break;
                }
            }
            totalRecord = getData.Count();
            totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);
            allData = getData.OrderByDescending(a => a.CustomerCode).Skip(((page - 1) * pageSize)).Take(pageSize).ToList();
            dbConn.Connection.Close();
            ViewBag.TotalRows = totalRecord;
            ViewBag.PageSize = pageSize;
            return View(allData);
        }

        public ActionResult GetCustomerFormNew()
        {
            CustomerInputModels model = new CustomerInputModels();
            model.CustomerCode = null;
            model.FistName = null;
            model.LastName = null;
            model.IDCrad = 0;
            model.Tel = null;
            model.IsActive = 1;

            return PartialView("_LayoutCustomerFormNew", model);
        }
        [HttpPost]
        public ActionResult SaveCustomerData(CustomerInputModels model, FormCollection form)
        {
            //insert database
           bool  cbxIsActiveBool = false;
            try
            {
                string dataBatchID = model.CustomerCode;
                string FistName = model.FistName;
                string LastName = model.LastName;
                int IDCrad = model.IDCrad;
                string Tel = model.Tel;
                string cbxIsActive = form["cbxIsActive"];
                //bool cbxIsActiveBool = Convert.ToBoolean(Request.Form["cbxIsActive"]);
                int IsActive = (cbxIsActive=="false") ? 0 : 1;

                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                dbDAC.mas_customer t = new dbDAC.mas_customer();
                var operCusCode = chawsuanObj.retCustomerCode();
                if (operCusCode.oper)
                {
                    string CustomerCode = operCusCode.retData;
                    t.CustomerCode = CustomerCode;
                    t.FistName = FistName;
                    t.LastName = LastName;
                    t.IDCrad = IDCrad;
                    t.Tel = Tel;
                    t.IsActive = IsActive;
                    dbConn.mas_customers.InsertOnSubmit(t);
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();
                    string insID = t.CustomerCode;
                    return Json(new { success = true, retData = cbxIsActive.ToString(), msg = "Record Inserted!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = operCusCode.retMsg }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetCustomerFormEdit(string cusID)
        {
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            CustomerInputModels model = new CustomerInputModels();
            model.CustomerCode = null;
            model.FistName = null;
            model.LastName = null;
            model.IDCrad = 0;
            model.Tel = null;
            model.IsActive = 1;
            var getData = (from c in dbConn.mas_customers where c.CustomerCode.Equals(cusID) select c);
            if (getData.Count() > 0)
            {
                var getDataUse = getData.FirstOrDefault();
                model.CustomerCode = getDataUse.CustomerCode;
                model.FistName = getDataUse.FistName;
                model.LastName = getDataUse.LastName;
                model.IDCrad = getDataUse.IDCrad.Value;
                model.Tel = getDataUse.Tel;
                model.IsActive = getDataUse.IsActive.Value;
            }
            dbConn.Connection.Close();
            return PartialView("_LayoutCustomerFormEdit", model);
        }
        [HttpPost]
        public ActionResult UpdateCustomerData(CustomerInputModels model, FormCollection form)
        {
            //insert database
            bool cbxIsActiveBool = false;
            try
            {
                string CustomerCode = model.CustomerCode;
                string FistName = model.FistName;
                string LastName = model.LastName;
                int IDCrad = model.IDCrad;
                string Tel = model.Tel;
                string cbxIsActive = form["cbxIsActive"];
                //bool cbxIsActiveBool = Convert.ToBoolean(Request.Form["cbxIsActive"]);
                int IsActive = (cbxIsActive == "false") ? 0 : 1;

                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.mas_customers where c.CustomerCode.Equals(CustomerCode) select c);
                if (getData.Count()>0)
                {
                    var getDataUse = getData.FirstOrDefault();
                    getDataUse.FistName = FistName;
                    getDataUse.LastName = LastName;
                    getDataUse.IDCrad = IDCrad;
                    getDataUse.Tel = Tel;
                    getDataUse.IsActive = IsActive;
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();
                    string insID = getDataUse.CustomerCode;
                    return Json(new { success = true, retData = cbxIsActive.ToString(), msg = "Record Update!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = "Data not found!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetCustomerFormDelete(string cusID)
        {
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            CustomerInputModels model = new CustomerInputModels();
            model.CustomerCode = null;
            model.FistName = null;
            model.LastName = null;
            model.IDCrad = 0;
            model.Tel = null;
            model.IsActive = 1;
            var getData = (from c in dbConn.mas_customers where c.CustomerCode.Equals(cusID) select c);
            if (getData.Count() > 0)
            {
                var getDataUse = getData.FirstOrDefault();
                model.CustomerCode = getDataUse.CustomerCode;
                model.FistName = getDataUse.FistName;
                model.LastName = getDataUse.LastName;
                model.IDCrad = getDataUse.IDCrad.Value;
                model.Tel = getDataUse.Tel;
                model.IsActive = getDataUse.IsActive.Value;
            }
            dbConn.Connection.Close();
            return PartialView("_LayoutCustomerFormDelete", model);
        }
        [HttpPost]
        public ActionResult DeleteCustomerData(CustomerInputModels model, FormCollection form)
        {
            try
            {
                string CustomerCode = model.CustomerCode;

                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.mas_customers where c.CustomerCode.Equals(CustomerCode) select c);
                if (getData.Count() > 0)
                {
                    var getDataUse = getData.FirstOrDefault();
                    dbConn.mas_customers.DeleteOnSubmit(getDataUse);
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();
                    string insID = getDataUse.CustomerCode;
                    return Json(new { success = true, retData = CustomerCode.ToString(), msg = "Record Deleted!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = "Data not found!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ProductManage(int page = 1, string SrcType = "Name", string SrcData = null)
        {
            ViewBag.Message = "Product Management.";
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            int pageSize = 10;
            int totalPage = 0;
            int totalRecord = 0;
            List<dbDAC.mas_product> allData = new List<dbDAC.mas_product>();
            var getData = (from t in dbConn.mas_products select t);
            if (!string.IsNullOrEmpty(SrcType) & !string.IsNullOrEmpty(SrcData))
            {
                switch (SrcType)
                {
                    case "Name":
                        getData = getData.Where(d => d.ProductName.Trim().Contains(SrcData));
                        break;
                }
            }
            totalRecord = getData.Count();
            totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);
            allData = getData.OrderByDescending(a => a.ProductCode).Skip(((page - 1) * pageSize)).Take(pageSize).ToList();
            dbConn.Connection.Close();
            ViewBag.TotalRows = totalRecord;
            ViewBag.PageSize = pageSize;
            return View(allData);
        }

        public ActionResult GetProductFormNew()
        {
            ProductInputModels model = new ProductInputModels();
            model.ProductCode = null;
            model.ProductName = null;
            model.ProductDescription = null;
            model.IsActive = 1;

            return PartialView("_LayoutProductFormNew", model);
        }
        [HttpPost]
        public ActionResult SaveProductData(ProductInputModels model, FormCollection form)
        {
            //insert database
            bool cbxIsActiveBool = false;
            try
            {
                //string ProductCode = model.ProductCode;
                string ProductName = model.ProductName;
                string ProductDescription = model.ProductDescription;
                string cbxIsActive = form["cbxIsActive"];
                //bool cbxIsActiveBool = Convert.ToBoolean(Request.Form["cbxIsActive"]);
                int IsActive = (cbxIsActive == "false") ? 0 : 1;

                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                dbDAC.mas_product t = new dbDAC.mas_product();
                var operProdCode = chawsuanObj.retProductCode();
                if (operProdCode.oper)
                {
                    string ProductCode = operProdCode.retData;
                    t.ProductCode = ProductCode;
                    t.ProductName = ProductName;
                    t.ProductDescription = ProductDescription;
                    t.IsActive = IsActive;
                    dbConn.mas_products.InsertOnSubmit(t);
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();
                    string insID = t.ProductCode;
                    return Json(new { success = true, retData = cbxIsActive.ToString(), msg = "Record Inserted!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = operProdCode.retMsg }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetProductFormEdit(string prodID)
        {
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            ProductInputModels model = new ProductInputModels();
            model.ProductCode = null;
            model.ProductName = null;
            model.ProductDescription = null;
            model.IsActive = 1;
            var getData = (from c in dbConn.mas_products where c.ProductCode.Equals(prodID) select c);
            if (getData.Count() > 0)
            {
                var getDataUse = getData.FirstOrDefault();
                model.ProductCode = getDataUse.ProductCode;
                model.ProductName = getDataUse.ProductName;
                model.ProductDescription = getDataUse.ProductDescription;
                model.IsActive = getDataUse.IsActive.Value;
            }
            dbConn.Connection.Close();
            return PartialView("_LayoutProductFormEdit", model);
        }
        [HttpPost]
        public ActionResult UpdateProductData(ProductInputModels model, FormCollection form)
        {
            //insert database
            bool cbxIsActiveBool = false;
            try
            {
                string ProductCode = model.ProductCode;
                string ProductName = model.ProductName;
                string ProductDescription = model.ProductDescription;
                string cbxIsActive = form["cbxIsActive"];
                //bool cbxIsActiveBool = Convert.ToBoolean(Request.Form["cbxIsActive"]);
                int IsActive = (cbxIsActive == "false") ? 0 : 1;

                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.mas_products where c.ProductCode.Equals(ProductCode) select c);
                if (getData.Count() > 0)
                {
                    var getDataUse = getData.FirstOrDefault();
                    getDataUse.ProductName = ProductName;
                    getDataUse.ProductDescription = ProductDescription;
                    getDataUse.IsActive = IsActive;
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();
                    string insID = getDataUse.ProductCode;
                    return Json(new { success = true, retData = insID.ToString(), msg = "Record Update!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = "Data not found!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetProductFormDelete(string prodID)
        {
            dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
            ProductInputModels model = new ProductInputModels();
            model.ProductCode = null;
            model.ProductName = null;
            model.ProductDescription = null;
            model.IsActive = 1;
            var getData = (from c in dbConn.mas_products where c.ProductCode.Equals(prodID) select c);
            if (getData.Count() > 0)
            {
                var getDataUse = getData.FirstOrDefault();
                model.ProductCode = getDataUse.ProductCode;
                model.ProductName = getDataUse.ProductName;
                model.ProductDescription = getDataUse.ProductDescription;
                model.IsActive = getDataUse.IsActive.Value;
            }
            dbConn.Connection.Close();
            return PartialView("_LayoutProductFormDelete", model);
        }
        [HttpPost]
        public ActionResult DeleteProductData(ProductInputModels model, FormCollection form)
        {
            try
            {
                string ProductCode = model.ProductCode;

                dbDAC.dbConceptualDataDataContext dbConn = new dbDAC.dbConceptualDataDataContext();
                var getData = (from c in dbConn.mas_products where c.ProductCode.Equals(ProductCode) select c);
                if (getData.Count() > 0)
                {
                    var getDataUse = getData.FirstOrDefault();
                    dbConn.mas_products.DeleteOnSubmit(getDataUse);
                    dbConn.SubmitChanges();
                    dbConn.Connection.Close();
                    string insID = getDataUse.ProductCode;
                    return Json(new { success = true, retData = ProductCode.ToString(), msg = "Record Deleted!!" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, retData = "NA", msg = "Data not found!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception exp)
            {
                return Json(new { success = false, retData = "NA", msg = exp.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}